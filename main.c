/*
The MIT License (MIT)

Copyright (c) 2019 Mikhail Paulyshka

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#define STBI_ONLY_JPEG
#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image.h"
#include "stb_image_write.h"

int main(int argc, char** argv)
{
	if(argc < 8){
		printf("<exe_name>.exe <1:input> <2:output> <3:pos_x> <4:pos_y> <5:len_x> <6:len_y> <7:quality>");
		return 1;
	}

	int pos_x = atoi(argv[3]);
	int pos_y = atoi(argv[4]);
	int len_x = atoi(argv[5]);
	int len_y = atoi(argv[6]);
	int quality = atoi(argv[7]);
	
	int image_width = 0, image_height = 0, channels = 0;
	unsigned char *data = stbi_load(argv[1], &image_width, &image_height, &channels, 0);
	if(!data){
		printf("failed to load image");
		return 2;
	}

	if((pos_x+len_x>image_width) || (pos_y+len_y>image_height)){
		printf("wrong dimensions");
		return 3;
	}

	unsigned char* data_out = malloc(len_x * len_y * channels);
	const int stride_size = image_width * channels;
	const int stride_size_out = len_x * channels;
	
	for(int i = 0, offset = pos_y * stride_size + pos_x * channels; i < len_y ; i++, offset += stride_size){
		memcpy(data_out + stride_size_out * i, data + offset, stride_size_out);
	}
	stbi_image_free(data);

	if (!stbi_write_jpg(argv[2], len_x, len_y, channels, data_out, quality))
	{
		printf("failed to save image");
		return 4;
	}
		
	return 0;
}
